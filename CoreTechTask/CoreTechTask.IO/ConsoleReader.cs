﻿using System;
namespace CoreTechTask.IO
{
    public class ConsoleReader
    {
        public string Read()
        {
            return Console.ReadLine();
        }
    }
}
