﻿using System;
using CoreTechTask.Core;
using CoreTechTask.IO;

namespace CoreTechTask
{
    class Program
    {
        private static readonly Factory factory = new Factory();
        private static readonly Database database = new Database();
        private static readonly ConsoleReader reader = new ConsoleReader();
        private static readonly ConsoleWriter writer = new ConsoleWriter();
        private static readonly Engine engine = new Engine(factory, database, reader, writer);

        static void Main()
        {
            engine.Run();
        }
    }
}
