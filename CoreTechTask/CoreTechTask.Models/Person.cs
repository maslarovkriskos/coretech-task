﻿using System;

namespace CoreTechTask.Models
{
    public class Person
    {
        private string name;
        private string gender;
        private int salary;

        public Person(string name, string gender, int salary)
        {
            this.Name = name;
            this.Gender = gender;
            this.Salary = salary;
        }

        public Person(string name, string gender, int salary, int incomeTax, int socialTax, int netSalary)
        {
            this.Name = name;
            this.Gender = gender;
            this.Salary = salary;
            this.IncomeTax = incomeTax;
            this.SocialTax = socialTax;
            this.NetSalary = netSalary;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Name cannot be empty.");
                }

                if (value.Length < 3 || value.Length > 10)
                {
                    throw new ArgumentException("Name should remain between 3 and 10 characters.");
                }

                this.name = value;
            }
        }

        public string Gender
        {
            get
            {
                return this.gender;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Gender cannot be empty.");
                }

                if (value.ToLower() != "male" && value.ToLower() != "female")
                {
                    throw new ArgumentException("Gender should be male or female.");
                }

                this.gender = value;
            }
        }

        public int Salary
        {
            get
            {
                return this.salary;
            }
            set
            {
                if (value < 324)
                {
                    throw new ArgumentException("Minimum salary is 324.");
                }

                this.salary = value;
            }
        }

        public int NetSalary { get; set; }

        public int IncomeTax { get; set; }

        public int SocialTax { get; set; }
    }
}
