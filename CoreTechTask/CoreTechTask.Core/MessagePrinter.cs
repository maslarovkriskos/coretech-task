﻿using System;
using CoreTechTask.IO;

namespace CoreTechTask.Core
{
    internal class MessagePrinter
    {
        public const string Delimiter = "===============================================================";

        private readonly ConsoleWriter writer;

        public MessagePrinter(ConsoleWriter writer)
        {
            this.writer = writer;
        }

        public void PrintMainMenu(string status)
        {
            writer.Clear();
            writer.WriteLine("            Enter \"exit\" to quit the application            ||");
            writer.WriteLine(Delimiter);
            writer.WriteLine("            Enter \"CreatePerson\" to create a board.           ||");
            writer.WriteLine(Delimiter);
            writer.WriteLine("            Enter \"ShowOnlyMale\" to list the male people.     ||");
            writer.WriteLine(Delimiter);
            writer.WriteLine("            Enter \"ShowOnlyFemale\" to list the female people.     ||");
            writer.WriteLine(Delimiter);
            writer.WriteLine("            Enter \"ListAllPeople\" to list all the people.      ||");
            writer.WriteLine(Delimiter);
            writer.WriteLine("            Enter \"ListLotteryPeople\" to list the people who can apply for the lottery.     ||");
            writer.WriteLine(Delimiter);
            writer.WriteLine("            Enter \"ShowLotteryWinner\" to show the randomly chosen person to win the lottery.     ||");
            writer.WriteLine(Delimiter);
            writer.WriteLine($"\nStatus: {status}\n");
            writer.WriteLine(Delimiter);
            writer.WriteLine();
            writer.Write(">");
        }

        public void PrintInputPrompts(string placeholder)
        {
            writer.Clear();
            writer.WriteLine(Delimiter);
            writer.WriteLine($"{placeholder}");
            writer.WriteLine(Delimiter);
            writer.Write(">");
        }
    }
}
