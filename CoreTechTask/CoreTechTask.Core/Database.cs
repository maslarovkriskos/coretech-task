﻿using System.Collections.Generic;
using CoreTechTask.Models;

namespace CoreTechTask.Core
{
    public class Database
    {
        public Database()
        {
            this.People = new List<Person>();

        }

        public IList<Person> People { get; }
    }
}
