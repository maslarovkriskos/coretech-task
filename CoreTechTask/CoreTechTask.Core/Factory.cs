﻿using System;
using System.Collections.Generic;
using System.Text;
using CoreTechTask.Models;

namespace CoreTechTask.Core
{
    public class Factory
    {
        public Person CreatePerson(string name, string gender, int salary)
        {
            var incomeTax = TaxCalculator.CalculateIncomeTax(salary);
            var socialTax = TaxCalculator.CalculateSocialTax(salary);
            var allTaxes = incomeTax + socialTax;
            var netSalary = salary - allTaxes;
            return new Person(name, gender, salary, incomeTax, socialTax, netSalary);
        }
    }
}
