﻿using System;

namespace CoreTechTask.Core
{
    public class TaxCalculator
    {
        public static int CalculateIncomeTax(int salary)
        {
            int incomeTax = 0;

            if (salary >= 1000)
            {
                incomeTax = (int)(salary * 0.10);
            }

            return incomeTax;
        }

        public static int CalculateSocialTax(int salary)
        {
            int salaryAfterIncomeTax = salary - TaxCalculator.CalculateIncomeTax(salary);
            int socialtax = 0;
            if (salaryAfterIncomeTax >= 1000 && salaryAfterIncomeTax <= 3000)
            {
                socialtax = (int)(salaryAfterIncomeTax * 0.15);
            }

            return socialtax;
        }
    }
}
