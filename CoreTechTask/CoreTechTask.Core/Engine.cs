﻿using System;
using CoreTechTask.Core.Commands;
using CoreTechTask.IO;

namespace CoreTechTask.Core
{
    public class Engine
    {
        private readonly Database database;
        private readonly Factory factory;
        private readonly CommandParser commandParser;
        private readonly ConsoleReader reader;
        private readonly ConsoleWriter writer;
        private readonly MessagePrinter printer;

        private string lastStatus = "Waiting for input. Enter to submit.";

        public Engine(Factory factory, Database database, ConsoleReader reader, ConsoleWriter writer)
        {
            this.writer = writer;
            this.reader = reader;
            this.factory = factory;
            this.database = database;
            this.printer = new MessagePrinter(writer);
            this.commandParser = new CommandParser(reader, writer);
        }

        public void Run()
        {
            RunMainMenu();
        }

        private void RunMainMenu()
        {
            printer.PrintMainMenu(lastStatus);

            string input = reader.Read();

            if (input.ToLower() == "exit")
            {
                Environment.Exit(0);
            }
            else if (input.ToLower() == "show commands")
            {
                lastStatus = "Waiting for input. Enter to submit.";
                RunMenu();
            }
            

            this.Parse(input);

            RunMainMenu();
        }

        

        private void RunMenu()
        {
            printer.PrintMainMenu(lastStatus);

            string input = reader.Read();

            if (input.ToLower() == "back")
            {
                RunMainMenu();
            }

            this.Parse(input);

            RunMenu();
        }

       

        private void Parse(string input)
        {
            try
            {
                Command command = this.commandParser.ParseCommand(input, database, factory);
                writer.Clear();

                this.lastStatus = command.Execute();

                writer.Write("Press enter to continue...");
                reader.Read();
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                this.lastStatus = $"ERROR: {e.Message}";
            }
        }
    }
}
