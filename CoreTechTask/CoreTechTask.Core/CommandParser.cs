﻿using System;
using System.Collections.Generic;
using CoreTechTask.Core.Commands;
using CoreTechTask.IO;

namespace CoreTechTask.Core
{
    public class CommandParser
    {
        private readonly ConsoleReader reader;
        private readonly ConsoleWriter writer;
        private readonly MessagePrinter printer;

        public CommandParser(ConsoleReader reader, ConsoleWriter writer)
        {
            this.writer = writer;
            this.reader = reader;
            this.printer = new MessagePrinter(writer);
        }

        public Command ParseCommand(string command, Database database, Factory factory)
        {
            List<string> parameters = new List<string>();

            switch (command)
            {
                // Create Commands
                case "CreatePerson":

                    printer.PrintInputPrompts("Enter Name:");
                    parameters.Add(reader.Read());
                    printer.PrintInputPrompts("Enter Gender(male or female):");
                    parameters.Add(reader.Read());
                    printer.PrintInputPrompts("Enter Salary:");
                    parameters.Add(reader.Read());
                    return new CreatePersonCommand(parameters, database, factory);

                case "ListAllPeople":

                    return new ListAllPeopleCommand(parameters, database, factory, writer);

                case "ListLotteryPeople":

                    return new ListLotteryPeopleCommand(parameters,database, factory, writer);

                case "ShowOnlyMale":

                    return new ShowOnlyMaleCommand(parameters, database, factory, writer);

                case "ShowOnlyFemale":

                    return new ShowOnlyFemaleCommand(parameters, database, factory, writer);

                case "ShowLotteryWinner":

                    return new ShowLotteryWinnerCommand(parameters, database, factory, writer);

                default:

                    throw new InvalidOperationException("Command does not exist");
            }
        }
    }
}
