﻿using System;
using System.Collections.Generic;
using System.Text;
using CoreTechTask.IO;
using CoreTechTask.Models;

namespace CoreTechTask.Core.Commands
{
    public class ListAllPeopleCommand : Command
    {
        private readonly ConsoleWriter writer;
        private List<Person> allPeopleList;

        public ListAllPeopleCommand(List<string> parameters, Database database, Factory factory, ConsoleWriter writer)
            : base(parameters, database, factory)
        {
            this.writer = writer;
            allPeopleList = new List<Person>();
        }

        public override string Execute()
        {
            foreach (var item in this.Database.People)
            {
                allPeopleList.Add(item);
            }

            if (allPeopleList.Count == 0)
            {
                return "No people in database!";
            }
            else
            {
                writer.WriteLine(PrintList());

                return $"Listed items.";
            }
        }

       
        private string PrintList()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in allPeopleList)
            {
                sb.AppendLine($"{item.Name} receives a salary of {item.Salary},has income tax: {item.IncomeTax}, has social tax:{item.SocialTax}, will bring home:{item.NetSalary}");
            }

            return sb.ToString();
        }
    }
}
