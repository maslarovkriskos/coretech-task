﻿using System.Collections.Generic;

namespace CoreTechTask.Core.Commands
{
    public abstract class Command
    {
        protected Command(List<string> parameters, Database database, Factory factory)
        {
            this.Parameters = new List<string>(parameters);
            this.Factory = factory;
            this.Database = database;
        }

        public abstract string Execute();

        protected List<string> Parameters { get; }

        protected Database Database { get; }

        protected Factory Factory { get; }
    }
}
