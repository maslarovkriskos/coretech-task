﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreTechTask.IO;
using CoreTechTask.Models;

namespace CoreTechTask.Core.Commands
{
    public class ShowLotteryWinnerCommand : Command
    {
        private readonly ConsoleWriter writer;
        private List<Person> allPeopleList;

        public ShowLotteryWinnerCommand(List<string> parameters, Database database, Factory factory, ConsoleWriter writer)
            : base(parameters, database, factory)
        {
            this.writer = writer;
            allPeopleList = new List<Person>();
        }

        public override string Execute()
        {
            allPeopleList = this.Database.People.Where(x => x.NetSalary <= 2500).ToList();

            if (allPeopleList.Count == 0)
            {
                return "No one has applied for the lottery!";
            }
            else
            {
                return writer.WriteLine(PrintList());
            }
        }


        private string PrintList()
        {
            StringBuilder sb = new StringBuilder();

            var random = new Random();
            var toWin = random.Next(0, allPeopleList.Count());
            var winner = allPeopleList[toWin];
            sb.AppendLine($"{winner.Name} has won the lottery this year!");
            

            return sb.ToString();
        }
    }
}
