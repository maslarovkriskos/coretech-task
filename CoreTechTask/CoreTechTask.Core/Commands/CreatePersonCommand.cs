﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreTechTask.Models;

namespace CoreTechTask.Core.Commands
{
    public class CreatePersonCommand : Command
    {
        public CreatePersonCommand(List<string> parameters, Database database, Factory factory)
            : base(parameters, database, factory)
        {
        }

        public override string Execute()
        {
            string name;
            string gender;
            string salary;

            try
            {
                name = this.Parameters[0];
                gender = this.Parameters[1];
                salary = this.Parameters[2];
            }
            catch
            {
                throw new ArgumentException("Could not parse CreateBoard parameters.");
            }


            Person person = this.Factory.CreatePerson(name, gender, int.Parse(salary));
            this.Database.People.Add(person);

            return $"{person.Gender} person with name {person.Name} and salary: {person.Salary} was created. \n\n  Enter new command...";
        }
    }
}
