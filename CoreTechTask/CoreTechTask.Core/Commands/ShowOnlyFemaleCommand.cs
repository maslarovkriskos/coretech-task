﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreTechTask.IO;
using CoreTechTask.Models;

namespace CoreTechTask.Core.Commands
{
    public class ShowOnlyFemaleCommand : Command
    {
        private readonly ConsoleWriter writer;
        private List<Person> allPeopleList;

        public ShowOnlyFemaleCommand(List<string> parameters, Database database, Factory factory, ConsoleWriter writer)
            : base(parameters, database, factory)
        {
            this.writer = writer;
            allPeopleList = new List<Person>();
        }

        public override string Execute()
        {
            allPeopleList = this.Database.People.Where(x => x.Gender.ToLower() == "female").ToList();

            if (allPeopleList.Count == 0)
            {
                return "There are no female people!";
            }
            else
            {
                writer.WriteLine(PrintList());

                return $"Listed items.";
            }
        }


        private string PrintList()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in allPeopleList)
            {
                sb.AppendLine($"{item.Name} receives a salary of {item.Salary},has income tax: {item.IncomeTax}, has social tax:{item.SocialTax}, will bring home:{item.NetSalary}");
            }

            return sb.ToString();
        }
    }
}
