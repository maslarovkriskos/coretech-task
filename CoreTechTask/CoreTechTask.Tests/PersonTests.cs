﻿using System;
using CoreTechTask.Core;
using CoreTechTask.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreTechTask.Tests
{
    [TestClass]
    public class PersonTests
    {
        [TestMethod]
        public void PersonHasName()
        {
            var person = new Person("Kristiyan", "Male", 1234);

            string expected = "Kristiyan";
            string actual = person.Name;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void PersonAddedToDatabaseCorrectly()
        {
            var person = new Person("Kristiyan", "Male", 1234);
            var db = new Database();
            db.People.Add(person);

            Assert.IsTrue(db.People.Contains(person));
          
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PersonNameIsNull()
        {
            var person = new Person(null, "Mal", 1234);
        }


        [TestMethod]
        public void PersonHasGender()
        {
            var person = new Person("Kristiyan", "Male", 1234);

            string expected = "male";
            string actual = person.Gender.ToLower();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PersonHasIncorrectGender()
        {
            var person = new Person("Kristiyan", "Mal", 1234);

            string expected = "male";
            string actual = person.Gender.ToLower();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void PersonHasSalary()
        {
            var person = new Person("Kristiyan", "Male", 1234);

            int expected = 1234;
            int actual = person.Salary;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PersonLowSalary()
        {
            var person = new Person("Kristiyan", "Male", 323);
        }
    }
}
