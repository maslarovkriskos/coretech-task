﻿using System;
using System.Collections.Generic;
using CoreTechTask.Core;
using CoreTechTask.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreTechTask.Tests
{
    [TestClass]
    public class DatabaseTests
    {
        [TestMethod]
        public void HasProperPeopleInstance()
        {
            Database database = new Database();

            Assert.IsTrue(database.People is List<Person>);
        }
    }
}
