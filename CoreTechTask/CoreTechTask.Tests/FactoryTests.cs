﻿using System;
using CoreTechTask.Core;
using CoreTechTask.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreTechTask.Tests
{
    [TestClass]
    public class FactoryTests
    {
        [TestMethod]
        public void CreatesPersonCorrectly()
        {
            Factory factory = new Factory();

            Assert.IsTrue(factory.CreatePerson("Kris", "male", 1234) is Person);
        }
    }
}
